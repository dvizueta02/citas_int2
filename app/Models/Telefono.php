<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Telefono extends Model
{
    use HasFactory;
    protected $table = 'telefono';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'operadora_tel',
        'costo_minuto_tel',
    ];
/*
       public function telefono() {
        return $this->belongsTo(Telefono::class);
        
*/


   /* public function paciente() {
        return $this->hasOne(Paciente::class);
        */



    }
}
