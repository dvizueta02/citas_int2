<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Horario_Atencion extends Model
{
    use HasFactory;
    protected $table = 'horarios_atencion';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'dia_horario_atencion',
        'inicio_horario_atencion',
        'fin_horario_atencion',
    ];
}
