<?php

namespace App\Http\Controllers;

use App\Models\Paciente;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class PacienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pacientes = Paciente::all();
     /*   $pacientes = Paciente::select('*')
                    ->with('telefono')
                    ->get()
        return $pacientes;
        */


        return view('pacientes.index', compact('pacientes'));
        //return $horarios;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pacientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     
     *******************************************************************************************    */



    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'nombres_paciente' => 'required',
            'email_paciente' => 'required',
            'ciudad_paciente' => 'required',
            'telefono_id' => 'required',

        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput();
        } else {
            Paciente::create($request->all());
            return redirect()->route('pacientes.index')->with('success', 'Horario Paciente creado exitosamente!!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Paciente $paciente)
    {
        return view('pacientes.edit', compact('paciente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $paciente = Paciente ::find($id);
        $paciente->update($request->all());

        return redirect()->route('pacientes.index')->with('success', 'Horario pacientes actualizado exitosamente!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $paciente = Paciente::find($id);
        $paciente->delete();

        return redirect()->route('pacientes.index')->with('success', 'Horario pacientes eliminado exitosamente!!');
    }
}
