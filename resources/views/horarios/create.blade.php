@extends('layouts.app')
@section('content')
    <div class="container">
        <div align="center">
            <h2>Agregar Nuevo Horario_11</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('horarios.index') }}"> Regresar</a>
        </div>
    </br>
        

        <form action="{{ route('horarios.store') }}" method="POST">
            @csrf


            <!-- **************************** inicio VALIDACIONES 1_ required-->
            <!-- 

            <div class="row">
                <div>
                    <div class="form-group">
                        <strong>Día Atención:</strong>
                        <input type="date" name="dia_horario_atencion" class="form-control"
                            placeholder="Ingrese el día de atención" 
                            value="{{ old('dia_horario_atencion') }}" required>  
                        @error('dia_horario_atencion')
                            <p class="error-message">{{ $message }}</p>
                        @enderror
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Hora Inicio de Atención:</strong>
                        <input type="time" name="inicio_horario_atencion" class="form-control"
                            value="{{ old('inicio_horario_atencion') }}" required>
                        @error('inicio_horario_atencion')
                            <p class="error-message">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
       
                <div>
                    <div class="form-group">
                        <strong>Hora Fin de Atención:</strong>
                        <input type="time" name="fin_horario_atencion" class="form-control"
                        value="{{ old('fin_horario_atencion') }}" required>
                        @error('fin_horario_atencion')
                        <p class="error-message">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                
                 -->
            <!-- ********************************  FIN_VALIDACIONES_1 required  -->


            <!-- ******************************** INICIO_VALIDACIONES_2-->
            
            <div class="row">
                <div>
                    <div class="form-group">
                        <strong>Día Atención:</strong>
                        <input type="date" name="dia_horario_atencion" class="form-control"
                            placeholder="Ingrese el día horario de atención" 
                           
                        @error('dia_horario_atencion')
                        <p>{{ $message }}</p>
                        @enderror
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Hora Inicio de Atención:</strong>
                        <input type="time" name="inicio_horario_atencion" class="form-control"
                        placeholder="Ingrese hora inicio día de atención" 

                        @error('inicio_horario_atencion')
                        <p>{{ $message }}</p>
                        @enderror
                    </div>
                </div>
       
                 <div>
                    <div class="form-group">
                        <strong>Hora Fin de Atención:</strong>
                        <input type="time" name="fin_horario_atencion" class="form-control"
                        placeholder="Ingrese hora fin de atención" 
        
                        @error('fin_horario_atencion')
                            <p>{{ $message }}</p>
                        @enderror
                    </div>
                </div>
            
            
               <!-- ******************************** FIN_VALIDACIONES_2-->

           

             
             <!-- ******************************** inicio milenght
            ingrese minimo dos caracteres- -->

                
                <div>
                </br> </br>
                    <div class="form-group">
                        <strong>Validación 1: "minlegth"  </strong>
                        <input type="text" name="fin_horario_atencion" class="form-control"
                        placeholder="Ingrese hora fin de atención" minlength="2">
                
                        @error('fin_horario_atencion') 
                            <p> {{ $message }}</p>
                        @enderror
                    </div>
                </div>
            
    <!-- ******************************** fin minlenght-->

 <!-- ******************************** inicio maxlenght
        Ingrese mMAXIMO 5 caracteres -->

                
            <div>
            </br> </br>
                <div class="form-group">
                    <strong>Validación 2: "maxlegth" > máximo 5 caracteres</strong>
                    <input type="text" name="fin_horario_atencion" class="form-control"
                    placeholder="Ingrese hora fin de atención" maxlength="5">
            
                    @error('fin_horario_atencion') 
                        <p> {{ $message }}</p>
                    @enderror
                </div>
            </div>
        
<!-- ******************************** fin minlenght-->

 <!-- ******************************** inicio maxlenght
        Ingrese solamente números- -->

                
        <div>
        </br> </br>
            <div class="form-group">
                <strong>Validación 3: "max" > ingrese máximo 5 caracteres "Solo numeros"</strong>
                <input type="number" name="fin_horario_atencion" class="form-control"
                placeholder="Ingrese hora fin de atención" max="5">
        
                @error('fin_horario_atencion') 
                    <p> {{ $message }}</p>
                @enderror
            </div>
        </div>
    
<!-- ******************************** fin maxlenght-->

 <!-- ******************************** inicio minlenght
        Ingrese solamente números- -->

                
        <div>
        </br> </br>
            <div class="form-group">
                <strong>Validación 4: "min" > ingrese mínimo 4 caracteres "Solo numeros"</strong>
                <input type="text" name="fin_horario_atencion" class="form-control"
                placeholder="Ingrese hora fin de atención" min="4">
        
                @error('fin_horario_atencion') 
                    <p> {{ $message }}</p>
                @enderror
            </div>
        </div>
    
<!-- ******************************** fin minlenght-->

 <!-- ******************************** inicio minlenght
        Validaciones con esilos-->

                
        <div>
        </br> </br>
            <div class="form-group">
                <strong>Validación 5: OBSERVACIONES:  Validaciones con estilos </strong>
                <input type="text" name="fin_horario_atencion" class="form-control"
                placeholder="Ingrese las observaciones" required>
        
                @error('fin_horario_atencion') 
                    <p> {{ $message }}</p>
                @enderror
            </div>
        </div>
    
<!-- ******************************** fin minlenght-->


 <!-- ******************************** inicio minlenght
        Validaciones con esilos-->

                
        <div>
        </br> </br>
            <div class="form-group">
                <strong>Validación 6: "pattern"  Tiene Discapacidad, escriba si o no </strong>
                <input type="text" name="fin_horario_atencion" class="form-control"
                placeholder="Ingrese las observaciones" pattern="si">
        
                @error('fin_horario_atencion') 
                    <p> {{ $message }}</p>
                @enderror
            </div>
        </div>
    
<!-- ******************************** fin minlenght-->

 <!-- ******************************** inicio minlenght
        Validaciones con esilos-->

                
        <div>
        </br> </br>
            <div class="form-group">
                <strong>Validación 7: "pattern" Ingrese el numero de telefono +123 123456789</strong>
                <input type="text" name="fin_horario_atencion" class="form-control"
                placeholder="Ingrese las observaciones" pattern="[+][0-5]{3}[][+][0-5]{3}+">
        
                @error('fin_horario_atencion') 
                    <p> {{ $message }}</p>
                @enderror
            </div>
        </div>
    
<!-- ******************************** fin minlenght-->

<!-- ******************************** inicio minlenght
        Validaciones con esilos-->

                
        <div>
        </br> </br>
            <div class="form-group">
                <strong>Validación cedula:  060427904-2</strong>
                <input type="text" name="fin_horario_atencion" class="form-control"
                placeholder="Ingrese las observaciones" pattern="[0-9]{9}">
        
                @error('fin_horario_atencion') 
                    <p> {{ $message }}</p>
                @enderror
            </div>
        </div>
    
<!-- ******************************** fin minlenght-->







                




                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                </br>  </br>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>








        </form>
    </div>
@endsection


