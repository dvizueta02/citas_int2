@extends('layouts.app')

@section('content')
    <div class="container">
        <br />
        <h1 align="center">REPORTE DE PACIENTES_ INDEX_1</h1>
        <div>
            <a class="btn btn-success" href="{{ route('pacientes.create') }}">Crear</a>
        </div>
        <br />
        @if (Session::get('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        <table class="table">
            <tr>
                <th>Id</th>
                <th>Nombres Paciente</th>
                <th>Email Paciente</th>
                <th>Ciudad Paciente</th>
                <th>Teléfono Paciente</th>
                <th>Acciones</th>
            </tr>
            @foreach ($pacientes as $paciente)
                <tr>
                    <td>{{ $paciente->id }}</td>
                    <td>{{ $paciente->nombres_paciente }}</td>
                    <td>{{ $paciente->email_paciente }}</td>
                    <td>{{ $paciente->ciudad_paciente }}</td>
                    <td>{{ $paciente->telefono_id }}</td>
                    <td> <a class="btn btn-primary" href="{{route('pacientes.edit',$paciente)}}" >
                        <i class="fa-solid fa-pen-to-square"> </i>
                        </a>
                        <form action="{{ route('pacientes.destroy',$paciente->id )}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-primary"> <i class="fas fa-trash-alt">  </i> </button>
                        </form>
                    </td>
                </tr>
            
            @endforeach
        </table>
    </div>
@endsection