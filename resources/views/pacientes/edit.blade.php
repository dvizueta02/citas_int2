@extends('layouts.app')
@section('content')




  <div class="container">
        <div align="center">
            <h2> Editar Horario de Atención_ 1</h2>
        </div>
   
     
            <div class="pull-right">
            </br>
                <a class="btn btn-primary" href="{{ route('pacientes.index') }}"> Regresar</a>
            </div>
        </div>
    </div>
</div>


</div>
    <form action="{{ route('pacientes.update',$paciente->id) }}" method="POST">
        @csrf
        @method('PUT')
         <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Nombre de paciente:</strong>
                    <input type="varchar" name="nombres_paciente" class="form-control" value="{{ $paciente->nombres_paciente }}">
                </div>
            </div>

            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Email de paciente:</strong>
                    <input type="varchar" name="email_paciente" class="form-control" value="{{ $paciente->email_paciente }}">
                </div>
            </div>

            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Ciudad Paciente:</strong>
                    <input type="varchar" name="ciudad_paciente" class="form-control" value="{{ $paciente->ciudad_paciente }}">
                </div>
            </div>

                <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                    <div class="form-group">
                        <strong>Telefono Paciente:</strong>
                        <input type="int" name="telefono_id" id="telefono_id" class="form-control" value="{{ $paciente->telefono_id }}">
                    </div>
                </div>


                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            </br>   </br>
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </form>
@endsection
