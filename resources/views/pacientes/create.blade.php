@extends('layouts.app')
@section('content')
    <div class="container">
        <div align="center">
            <h2>Agregar Nuevo Paciente_ Crear </h2>
        </div>
        <div class="pull-right">
        </br>
            <a class="btn btn-primary" href="{{ route('pacientes.index') }}"> Regresar</a>
        
        </br> </br> 
        </div>

        <form action="{{ route('pacientes.store') }}" method="POST">
            @csrf

            <div class="row">
                <div>
                    <div class="form-group">
                        <strong>Nombre Paciente :</strong>
                        <input type="varchar" name="nombres_paciente" class="form-control"
                            placeholder="Diego Vizueta" value="{{ old('nombres_paciente') }}" required>
                        @error('nombres_paciente')
                            <p class="error-message">{{ $message }}</p>
                        @enderror
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Email Paciente:</strong>
                        <input type="varchar" name="email_paciente" class="form-control"
                        placeholder="diegovizueta@gmail.com"
                            value="{{ old('email_paciente') }}" required>
                        @error('email_paciente')
                            <p class="error-message">{{ $message }}</p>
                        @enderror
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Ciudad Paciente:</strong>
                        <input type="varchar" name="ciudad_paciente" class="form-control"
                        placeholder="Quito"
                            value="{{ old('ciudad_paciente') }}" required>
                        @error('ciudad_paciente')
                            <p class="error-message">{{ $message }}</p>
                        @enderror
                    </div>
                </div>

               
                <div>
                    <div class="form-group">
                        <strong>Telefono Paciente:</strong>
                        <input type="tel" name="telefono_id" class="form-control"
                        placeholder="0984649924"
                        value="{{ old('telefono_id ') }}" required>
                         <!--    value="{{ old('telefono_id ') }}" pattern="[+][0-5]{3}[0-9]{9}"> -->
                        @error('telefono_id ')
                            <p class="error-message">{{ $message }}</p>
                        @enderror
                    </div>
                </div> 
            
             <!--  -->
     
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                </br>  </br>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
    </div>
@endsection
