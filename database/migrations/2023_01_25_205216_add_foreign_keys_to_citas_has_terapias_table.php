<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('citas_has_terapias', function (Blueprint $table) {
            $table->foreign(['citas_id_cita'], 'fk_citas_has_terapias_citas1')->references(['id_cita'])->on('citas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign(['terapias_id_terapia'], 'fk_citas_has_terapias_terapias1')->references(['id_terapia'])->on('terapias')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('citas_has_terapias', function (Blueprint $table) {
            $table->dropForeign('fk_citas_has_terapias_citas1');
            $table->dropForeign('fk_citas_has_terapias_terapias1');
        });
    }
};
